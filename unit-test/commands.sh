#! /bin/bash
mkdir -p ~/projects/stam/03/unit-test
code ~/projects/stam/03/unit-test

git init
npm init -y

npm i --save express
npm i --save-dev mocha chai

echo "node_modules" > .gitignore

git remote add origin https://gitlab.com/nanastusGroup/unit-test
git push -u origin main

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js
