import { assert, expect, should } from "chai";
import { describe } from "mocha";
import calc from "../src/calc.js";

describe("calc.js", () => {
    let myVar;
    before(() => {
        myVar = 0;
    });
    beforeEach(() => {
        myVar++;
    });
    after(() => console.log(`myVar: ${myVar}`));
    it("can add numbers", () => {
        expect(calc.add(2,2)).to.equal(4);
        expect(calc.add(2,-2)).to.equal(0);
    });
    it("can substract numbers",() => {
        expect(calc.subtract(5,1)).to.equal(4);
        expect(calc.subtract(-5,1)).to.equal(-6);
        assert(calc.subtract(5,2) == 3);
        assert(calc.subtract(5,-2) == 7);
    });
    it("can multiply numbers", () => {
        expect(calc.multiply(2,2)).to.equal(4);
        should().exist(calc.multiply);
        calc.multiply(2,2).should.equal(4);
    });
    it("can divide numbers", () => {
        expect(calc.division(4,2)).to.equal(2);
    });
    it("zero division throws an error", () => {
        const err_msg = "0 division not allowed";
        expect(() => calc.division(1,0))
            .to
            .throw(err_msg);
    });
});

