// src/calc.js

/**
 * Adds two numbers together.
 * @param {number} a
 * @param {number} b
 * @returns {number}
 */
const add = (a,b) => a+b;

/**
 * Substracts number from minuend.
 * @param {number} minuend
 * @param {number} subrahend
 * @returns {number} difference
 */
const subtract = (minuend, subrahend) => {
    return minuend - subrahend;
}

/**
 * Multiplys two numbers.
 * @param {number} multiplier
 * @param {number} multiplicant
 * @returns {number} multiplication
 */
const multiply = (multiplier,multiplicant) => multiplier*multiplicant;

/**
 * Divides two numbers.
 * @param {number} dividend
 * @param {number} divisor
 * @returns {number} division result
 * @throws {Error} 0 division
 */
const division = (dividend, divisor) => {
    if(divisor == 0) throw new Error("0 division not allowed"); {
        const fraction = dividend/divisor;
        return fraction;
    }
}

export default {add, subtract, multiply, division};